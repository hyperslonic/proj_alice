import pandas as pd
import numpy as np
from sklearn.metrics import log_loss

import math
def dpcg(df,col='yhat_tmp'):
    scores = []
    df2 = df.copy()
    for g,r in df2.groupby('context_id'):
        r = r.sort_values('reply_id')
        lbls = r.ylabel.values.tolist()
        r = r.sort_values(col, ascending=False)
        dp = r.reply_id.values.tolist()
        i = 0
        score = 0
        for i in range(len(lbls)):
            score += lbls[dp[i]] / math.log2(2+i)
        iscore = 0
        lbls.sort()
        lbls.reverse()
        for i in range(len(lbls)):
            iscore += lbls[i] / math.log2(2+i)
        if iscore > 0:
            scores.append(score/iscore)
    return np.mean(scores) 


d = []
for f in open('../input/train.tsv', encoding="utf-8"):
    ss = f.split('\t')
    ss[-1] = ss[-1].strip()
    if (len(ss)!=8):
        print(f)
        print(ss)
    d.append(ss) 
train = pd.DataFrame(d, columns=['context_id','context_2','context_1','context_0','reply_id','reply','label','confidence'])
train['context_id'] = train['context_id'].astype(np.int64)
train['reply_id'] = train['reply_id'].astype('int')
train['ylabel'] = train.label.replace({'good':2,'neutral':1,'bad':0}).values.tolist()

d = []
for f in open('../input/final.tsv', encoding="utf-8"):
    ss = f.split('\t')
    ss[-1] = ss[-1].strip()
    if (len(ss)!=6):
        print(f)
        print(ss)
    d.append(ss) 
test = pd.DataFrame(d, columns=['context_id','context_2','context_1','context_0','reply_id','reply'])
test['context_id'] = test['context_id'].astype(np.int64)
test['reply_id'] = test['reply_id'].astype('int')


train['bad'] = np.where(train.label == 'bad',1,0)
train['neutral'] = np.where(train.label == 'neutral',1,0)
train['good'] = np.where(train.label == 'good',1,0) 

train['context'] = train.context_2 + ' ' + train.context_1 + ' ' + train.context_0
test['context'] = test.context_2 + ' ' + test.context_1 + ' ' + test.context_0

print('loading bpe...')
qtrain = pd.read_csv('../input/train_bpe.csv')
qtrain.columns = ['context_id','reply_id','context_bpe','reply_bpe']
train = train.merge(qtrain, on=['context_id','reply_id'], how='left')
qtest = pd.read_csv('../input/final_bpe.csv')
qtest.columns = ['context_id','reply_id','context_bpe','reply_bpe']
test = test.merge(qtest, on=['context_id','reply_id'], how='left')

def parse_int_list(s):
    try:
        s = s.strip()[1:-1].split(',')
    except:
        print(s)
    res = []
    try:
        res = [int(q) for q in s]
    except:
        print(s)
    return res

for c in ['reply_bpe','context_bpe']:
    train[c] = train[c].map(parse_int_list)
    test[c] = test[c].map(parse_int_list)    

import gc
print("Text to seq process...")
print("   Fitting tokenizer...")
from keras.preprocessing.text import Tokenizer
tok_raw = Tokenizer(num_words=200000, filters='#$%&()*+-/:;<=>@[\]^_`{|}~')
df = test.copy()
df = pd.concat([train,test],ignore_index=True).reset_index(drop=True)
tok_raw.fit_on_texts(df.context_2.astype('str') + ' ' + df.context_1.astype('str') + ' ' + df.context_0.astype('str') + ' ' +df.reply.astype('str'))
del df
gc.collect()

num_words = len(tok_raw.word_index)
print('num words:', num_words)

embed_size = 300

print('loading embeddings rusvec vectors')
def get_coefs(word,*arr): 
    return word, np.asarray(arr, dtype='float32')
embeddings_index_rusvec = {}
for o in open('../input/ruwikiruscorpora_upos_skipgram_300_2_2018.vec', encoding="utf8"):
    try:
        w,v = get_coefs(*o.strip().split(' '))
        w = w.split('_')[0]
        if len(v) == 300:
            embeddings_index_rusvec[w] = v
    except:
        print(o) 

all_embs = np.stack(embeddings_index_rusvec.values())
emb_mean,emb_std = all_embs.mean(), all_embs.std()         

print('create embedding matrix rusvec')
word_index = tok_raw.word_index
nb_words = len(word_index) + 1
embedding_matrix_rusvec = np.random.normal(emb_mean, emb_std, (nb_words, embed_size))
for word, i in word_index.items():
    embedding_vector = embeddings_index_rusvec.get(word)
    try:
        if embedding_vector is not None: embedding_matrix_rusvec[i] = embedding_vector
    except:
        print(i,word)
del embeddings_index_rusvec, all_embs, emb_mean, emb_std
gc.collect()

print('loading embeddings fasttext vectors')
def get_coefs(word,*arr): 
    return word, np.asarray(arr, dtype='float32')
embeddings_index = {}
for o in open('../input/wiki.ru.vec', encoding="utf8"):
    try:
        w,v = get_coefs(*o.strip().split(' '))
        w = w.split('_')[0]
        if len(v) == 300:
            embeddings_index[w] = v
    except:
        print(o)     

all_embs = np.stack(embeddings_index.values())
emb_mean,emb_std = all_embs.mean(), all_embs.std()        

print('create embedding matrix fasttext')
word_index = tok_raw.word_index
nb_words = len(word_index) + 1
embedding_matrix = np.random.normal(emb_mean, emb_std, (nb_words, embed_size))
for word, i in word_index.items():
    embedding_vector = embeddings_index.get(word)
    try:
        if embedding_vector is not None: embedding_matrix[i] = embedding_vector
    except:
        print(i,word)
del embeddings_index, all_embs, emb_mean, emb_std
gc.collect()

print('loading matrix bpe')
embedding_matrix2 = np.load('../input/matrix.npy')
num_words2 = embedding_matrix2.shape[0]
print('num words 2:', num_words2)   

print("   Transforming text to seq...")
for c in ['context','reply']:
    train[c] = tok_raw.texts_to_sequences(train[c])
    test[c] = tok_raw.texts_to_sequences(test[c])
   
from sklearn.preprocessing import LabelEncoder, MinMaxScaler, StandardScaler
train['rep_len'] = train.reply.apply(len)
ss1 = MinMaxScaler()
train['rep_len'] = ss1.fit_transform(train.rep_len.values.reshape(-1, 1))
test['rep_len'] = test.reply.apply(len)
test['rep_len'] = ss1.transform(test.rep_len.values.reshape(-1, 1))

from keras.preprocessing.sequence import pad_sequences
def get_keras_data(dataset):
    X = {
        'context': pad_sequences(dataset.context, maxlen=40),
        'reply': pad_sequences(dataset.reply, maxlen=25),
        'context_rusvec': pad_sequences(dataset.context, maxlen=40),
        'reply_rusvec': pad_sequences(dataset.reply, maxlen=25),
        'context_bpe': pad_sequences(dataset.context_bpe, maxlen=40),
        'reply_bpe': pad_sequences(dataset.reply_bpe, maxlen=25)
    }
    return X

from keras.layers import Input, Dropout, Dense, BatchNormalization,SpatialDropout1D, \
    Activation, concatenate, GRU, Embedding, Flatten, CuDNNGRU,Bidirectional,GlobalMaxPooling1D,multiply,Conv1D
from keras.models import Model
from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping#, TensorBoard
from keras import backend as K
from keras import optimizers
from keras import initializers

dr = 0.25
def get_model():
    #params
    dr_r = dr
    
    #Inputs
    context = Input(shape=[x_train["context"].shape[1]], name="context")
    reply = Input(shape=[x_train["reply"].shape[1]], name="reply")
    context_bpe = Input(shape=[x_train["context_bpe"].shape[1]], name="context_bpe")
    reply_rusvec = Input(shape=[x_train["reply_rusvec"].shape[1]], name="reply_rusvec")
    context_rusvec = Input(shape=[x_train["context_rusvec"].shape[1]], name="context_rusvec")
    reply_bpe = Input(shape=[x_train["reply_bpe"].shape[1]], name="reply_bpe")
    
    #Embeddings layers
    emb_context = SpatialDropout1D(0.1)(Embedding(nb_words, 300, weights=[embedding_matrix], trainable=False)(context))
    emb_reply = SpatialDropout1D(0.1)(Embedding(nb_words, 300, weights=[embedding_matrix], trainable=False)(reply))
    emb_context_rusvec = SpatialDropout1D(0.1)(Embedding(nb_words, 300, weights=[embedding_matrix_rusvec], trainable=False)(context_rusvec))
    emb_reply_rusvec = SpatialDropout1D(0.1)(Embedding(nb_words, 300, weights=[embedding_matrix_rusvec], trainable=False)(reply_rusvec))
    emb_context_bpe = SpatialDropout1D(0.1)(Embedding(num_words2, 50, weights=[embedding_matrix2], trainable=False)(context_bpe))
    emb_reply_bpe = SpatialDropout1D(0.1)(Embedding(num_words2, 50, weights=[embedding_matrix2], trainable=False)(reply_bpe))
    
    rnn_layer1 = Bidirectional(CuDNNGRU(32, return_sequences=True)) (emb_context)
    rnn_layer2 = Bidirectional(CuDNNGRU(32, return_sequences=True))  (emb_reply)
    rnn_layer3 = Bidirectional(CuDNNGRU(32, return_sequences=True)) (emb_context_rusvec)
    rnn_layer4 = Bidirectional(CuDNNGRU(32, return_sequences=True))  (emb_reply_rusvec)
    rnn_layer5 = Bidirectional(CuDNNGRU(32, return_sequences=True)) (emb_context_bpe) 
    rnn_layer6 = Bidirectional(CuDNNGRU(32, return_sequences=True))  (emb_reply_bpe)
    
    #main layer
    main_l1 = multiply([
        GlobalMaxPooling1D()(rnn_layer1),
        GlobalMaxPooling1D()(rnn_layer2)
    ])
    main_l2 = multiply([
        GlobalMaxPooling1D()(rnn_layer4),
        GlobalMaxPooling1D()(rnn_layer5)
    ])
    main_l3 = multiply([
        GlobalMaxPooling1D()(rnn_layer5),
        GlobalMaxPooling1D()(rnn_layer6)
    ])
    main_l = concatenate([main_l1,main_l2,main_l3])
    main_l = Dropout(0.4)(main_l)
    
    #output
    output = Dense(3,activation="sigmoid") (main_l)
    
    #model
    model = Model([context, reply, context_bpe, reply_rusvec, context_rusvec, reply_bpe], output)
    optimizer = optimizers.RMSprop()
    #optimizer = optimizers.Adam(lr=0.0008)
    model.compile(loss="binary_crossentropy", 
                  optimizer=optimizer)
    return model

def _train_model(model, batch_size, train_x, train_y, val_x, val_y):
    best_loss = -1
    best_weights = None
    best_epoch = 0

    current_epoch = 0

    while True:
        #K.set_value(model.optimizer.lr, 0.001 * (0.9**current_epoch)) 
        model.fit(train_x, train_y, batch_size=batch_size, epochs=1)
        y_pred = model.predict(val_x, batch_size=batch_size)

        total_loss = 0
        for j in range(3):
            loss = log_loss(val_y[:, j], y_pred[:, j])
            total_loss += loss

        total_loss /= 3.

        print("Epoch {0} loss {1} best_loss {2}".format(current_epoch, total_loss, best_loss))

        if (np.isnan(total_loss)):
            break

        current_epoch += 1
        if total_loss < best_loss or best_loss == -1:
            best_loss = total_loss
            best_weights = model.get_weights()
            best_epoch = current_epoch
        else:
            if current_epoch - best_epoch == 5:
                break

    model.set_weights(best_weights)
    return model    

import random
uids = train.context_id.unique().tolist()
#random.shuffle(uids)
BATCH_SIZE = 1024

for c in ['pgood','pneutral','pbad']:
    train[c] = 0
    test[c] = 0
x_test = get_keras_data(test)

from sklearn.model_selection import KFold
kf = KFold(n_splits=10, shuffle=True, random_state=239)
ifold  = 0
for train_index,test_index in kf.split(uids):
    val_ids = [uids[q] for q in test_index]
    y_train,y_valid = train.loc[~train.context_id.isin(val_ids),['good','neutral','bad']].values,train.loc[train.context_id.isin(val_ids),['good','neutral','bad']].values
    x_train,x_valid = get_keras_data(train.loc[~train.context_id.isin(val_ids),:]),get_keras_data(train.loc[train.context_id.isin(val_ids),:])
    for k in range(1):
        K.clear_session()
        model = get_model()
        model = _train_model(model, BATCH_SIZE, x_train, y_train, x_valid, y_valid)
        train.loc[train.context_id.isin(val_ids),['pgood','pneutral','pbad']] += model.predict(x_valid, batch_size=BATCH_SIZE)
        p = model.predict(x_test, batch_size=BATCH_SIZE)
        i = 0
        for c in ['pgood','pneutral','pbad']:
            test[c] += p[:,i]*0.1
            i = i+1
    train['clas_rank'] = train.pgood + train.pneutral - train.pbad
    print('*'*30)
    print(ifold, dpcg(train.loc[train.context_id.isin(val_ids),:],'clas_rank'))
    ifold += 1

qq = train[['context_id','reply_id','pgood','pbad','pneutral']].copy()
qq.columns = ['context_id','reply_id','cl_good_ftrvbpe','cl_bad_ftrvbpe','cl_neutral_ftrvbpe']
qq.to_csv('train_clas_ft_rv_bpe.csv',index=False)

qq = test[['context_id','reply_id','pgood','pbad','pneutral']].copy()
qq.columns = ['context_id','reply_id','cl_good_ftrvbpe','cl_bad_ftrvbpe','cl_neutral_ftrvbpe']
qq.to_csv('test_clas_ft_rv_bpe.csv',index=False)        