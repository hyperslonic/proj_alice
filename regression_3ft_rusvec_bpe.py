import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder, MinMaxScaler, StandardScaler
import math

d = []
for f in open('../input/train.tsv', encoding="utf-8"):
    ss = f.split('\t')
    ss[-1] = ss[-1].strip()
    if (len(ss)!=8):
        print(f)
        print(ss)
    d.append(ss) 
train = pd.DataFrame(d, columns=['context_id','context_2','context_1','context_0','reply_id','reply','label','confidence'])
train['context_id'] = train['context_id'].astype(np.int64)
train['reply_id'] = train['reply_id'].astype('int')

train['repl_full_len'] = train.reply.apply(len)
train['y'] = train.confidence.astype(np.float32)*train['label'].replace({'good':1,'bad':-1.2,'neutral':0.5})
train.loc[train.label=='neutral','y'] = train.loc[train.label=='neutral','y'] - 0.5
train['reply_id'] = train.reply_id.astype('int')

d = []
for f in open('../input/final.tsv', encoding="utf-8"):
    ss = f.split('\t')
    ss[-1] = ss[-1].strip()
    if (len(ss)!=6):
        print(f)
        print(ss)
    d.append(ss) 
test = pd.DataFrame(d, columns=['context_id','context_2','context_1','context_0','reply_id','reply'])
test['context_id'] = test['context_id'].astype(np.int64)
test['reply_id'] = test['reply_id'].astype('int')
test['repl_full_len'] = test.reply.apply(len)

train['context'] = train.context_0 + ' ' + train.context_1 + ' ' + train.context_2
test['context'] = test.context_0 + ' ' + test.context_1 + ' ' + test.context_2

stopwords = []
for f in open('../input/stopwords-ru.txt', encoding="utf-8"):
    stopwords.append(f.strip())
def cnt_stopwords(s):
    ss = s.strip().split(' ')
    sws = [q for q in ss if q in stopwords]
    return len(sws)
train['cnt_stopwords'] = train.reply.apply(cnt_stopwords)
test['cnt_stopwords'] = test.reply.apply(cnt_stopwords)
train['rel_stops'] = train.cnt_stopwords / (1 + train.reply.astype('str').str.count('\S+'))
test['rel_stops'] = test.cnt_stopwords / (1 + test.reply.astype('str').str.count('\S+'))
ss5 = MinMaxScaler()
ss6 = MinMaxScaler()
train['cnt_stopwords'] = ss5.fit_transform(train.cnt_stopwords.values.reshape(-1, 1))
train['rel_stops'] = ss6.fit_transform(train.rel_stops.values.reshape(-1, 1))
test['cnt_stopwords'] = ss5.transform(test.cnt_stopwords.values.reshape(-1, 1))
test['rel_stops'] = ss6.transform(test.rel_stops.values.reshape(-1, 1))

def word_match_share(row):
    q1words = {}
    q2words = {}
    for word in str(row['context']).split():
        if word not in stopwords:
            q1words[word] = 1
    for word in str(row['reply']).split():
        if word not in stopwords:
            q2words[word] = 1
    if len(q1words) == 0 or len(q2words) == 0:
        # The computer-generated chaff includes a few questions that are nothing but stopwords
        return 0
    shared_words_in_q1 = [w for w in q1words.keys() if w in q2words]
    shared_words_in_q2 = [w for w in q2words.keys() if w in q1words]
    R = (len(shared_words_in_q1) + len(shared_words_in_q2))/(len(q1words) + len(q2words))
    return R
def word_match_share0(row):
    q1words = {}
    q2words = {}
    for word in str(row['context_0']).split():
        if word not in stopwords:
            q1words[word] = 1
    for word in str(row['reply']).split():
        if word not in stopwords:
            q2words[word] = 1
    if len(q1words) == 0 or len(q2words) == 0:
        # The computer-generated chaff includes a few questions that are nothing but stopwords
        return 0
    shared_words_in_q1 = [w for w in q1words.keys() if w in q2words]
    shared_words_in_q2 = [w for w in q2words.keys() if w in q1words]
    R = (len(shared_words_in_q1) + len(shared_words_in_q2))/(len(q1words) + len(q2words))
    return R
train['rep_con_share'] = train.apply(word_match_share,axis=1)
test['rep_con_share'] = test.apply(word_match_share,axis=1)
train['rep_con_share_0'] = train.apply(word_match_share0,axis=1)
test['rep_con_share_0'] = test.apply(word_match_share0,axis=1)

def jaro_winkler(row): #, long_tolerance, winklerize):
    ying, yang = row['context_0'],row['reply']
    #ying, yang = ' '.join([q for q in ying.split(' ') if q not in stopwords]),' '.join([q for q in yang.split(' ') if q not in stopwords])
    winklerize=True
    long_tolerance=True
    ying_len = len(ying)
    yang_len = len(yang)

    if not ying_len or not yang_len:
        return 0.0

    min_len = max(ying_len, yang_len)
    search_range = (min_len // 2) - 1
    if search_range < 0:
        search_range = 0

    ying_flags = [False]*ying_len
    yang_flags = [False]*yang_len

    # looking only within search range, count & flag matched pairs
    common_chars = 0
    for i, ying_ch in enumerate(ying):
        low = i - search_range if i > search_range else 0
        hi = i + search_range if i + search_range < yang_len else yang_len - 1
        for j in range(low, hi+1):
            if not yang_flags[j] and yang[j] == ying_ch:
                ying_flags[i] = yang_flags[j] = True
                common_chars += 1
                break

    # short circuit if no characters match
    if not common_chars:
        return 0.0

    # count transpositions
    k = trans_count = 0
    for i, ying_f in enumerate(ying_flags):
        if ying_f:
            for j in range(k, yang_len):
                if yang_flags[j]:
                    k = j + 1
                    break
            if ying[i] != yang[j]:
                trans_count += 1
    trans_count /= 2

    # adjust for similarities in nonmatched characters
    common_chars = float(common_chars)
    weight = ((common_chars/ying_len + common_chars/yang_len +
              (common_chars-trans_count) / common_chars)) / 3

    # winkler modification: continue to boost if strings are similar
    if winklerize and weight > 0.7 and ying_len > 3 and yang_len > 3:
        # adjust for up to first 4 chars in common
        j = min(min_len, 4)
        i = 0
        while i < j and ying[i] == yang[i] and ying[i]:
            i += 1
        if i:
            weight += i * 0.1 * (1.0 - weight)

        # optionally adjust for long strings
        # after agreeing beginning chars, at least two or more must agree and
        # agreed characters must be > half of remaining characters
        if (long_tolerance and min_len > 4 and common_chars > i+1 and
                2 * common_chars >= min_len + i):
            weight += ((1.0 - weight) * (float(common_chars-i-1) / float(ying_len+yang_len-i*2+2)))

    return weight
train['dist_jaro'] = train.apply(jaro_winkler,axis=1)
test['dist_jaro'] = test.apply(jaro_winkler,axis=1)

from collections import Counter
from difflib import SequenceMatcher
def get_cosine(row):
    str1 = row['context']
    str2 = row['reply']

    vec1 = Counter(str1.split())
    vec2 = Counter(str2.split())

    intersection = set(vec1.keys()) & set(vec2.keys())
    numerator = sum([vec1[x] * vec2[x] for x in intersection])

    sum1 = sum([vec1[x]**2 for x in vec1.keys()])
    sum2 = sum([vec2[x]**2 for x in vec2.keys()])
    denominator = math.sqrt(sum1) * math.sqrt(sum2)

    if not denominator:
        return 0.0
    else:
        return float(numerator) / denominator

def DistJaccard(row):
    str1 = row['context']
    str2 = row['reply']    
    str1 = set(str1.split())
    str2 = set(str2.split())
    numerator = len(str1 & str2)
    denominator = len(str1 | str2)
    if not denominator:
        return 0.0
    else:
        return float(numerator) / denominator
def get_cosine0(row):
    str1 = row['context_0']
    str2 = row['reply']

    vec1 = Counter(str1.split())
    vec2 = Counter(str2.split())

    intersection = set(vec1.keys()) & set(vec2.keys())
    numerator = sum([vec1[x] * vec2[x] for x in intersection])

    sum1 = sum([vec1[x]**2 for x in vec1.keys()])
    sum2 = sum([vec2[x]**2 for x in vec2.keys()])
    denominator = math.sqrt(sum1) * math.sqrt(sum2)

    if not denominator:
        return 0.0
    else:
        return float(numerator) / denominator

def DistJaccard0(row):
    str1 = row['context_0']
    str2 = row['reply']    
    str1 = set(str1.split())
    str2 = set(str2.split())
    numerator = len(str1 & str2)
    denominator = len(str1 | str2)
    if not denominator:
        return 0.0
    else:
        return float(numerator) / denominator    
    
train['dist_cos'] = train.apply(get_cosine,axis=1)
test['dist_cos'] = test.apply(get_cosine,axis=1)    
train['dist_jac'] = train.apply(DistJaccard,axis=1)
test['dist_jac'] = test.apply(DistJaccard,axis=1)    
train['dist_cos0'] = train.apply(get_cosine0,axis=1)
test['dist_cos0'] = test.apply(get_cosine0,axis=1)    
train['dist_jac0'] = train.apply(DistJaccard0,axis=1)
test['dist_jac0'] = test.apply(DistJaccard0,axis=1)    



import gc
print("Text to seq process...")
print("   Fitting tokenizer...")
from keras.preprocessing.text import Tokenizer
tok_raw = Tokenizer(num_words=200000, filters='#$%&()*+-/:;<=>@[\]^_`{|}~')
df = test.copy()
df = pd.concat([train,test],ignore_index=True).reset_index(drop=True)
tok_raw.fit_on_texts(df.context_2.astype('str') + ' ' + df.context_1.astype('str') + ' ' + df.context_0.astype('str') + ' ' +df.reply.astype('str'))
del df
gc.collect()

num_words = len(tok_raw.word_index)

oov = []
embed_size = 300

print('loading fastext embeddings vectors')
def get_coefs(word,*arr): 
    return word, np.asarray(arr, dtype='float32')
embeddings_index = {}
for o in open('../input/wiki.ru.vec', encoding="utf8"):
    try:
        w,v = get_coefs(*o.strip().split(' '))
        if len(v) == 300:
            embeddings_index[w] = v
    except:
        print(o)

all_embs = np.stack(embeddings_index.values())
emb_mean,emb_std = all_embs.mean(), all_embs.std()

print('create embedding matrix')
word_index = tok_raw.word_index
nb_words = len(word_index) + 1
embedding_matrix = np.random.normal(emb_mean, emb_std, (nb_words, embed_size))
for word, i in word_index.items():
    embedding_vector = embeddings_index.get(word)
    try:
        if embedding_vector is not None: embedding_matrix[i] = embedding_vector
        else: oov.append(word)
    except:
        print(word,i)
        pass

del embeddings_index, all_embs, emb_std, emb_mean
gc.collect()

print('loading rusvec embeddings vectors')
def get_coefs(word,*arr): 
    return word, np.asarray(arr, dtype='float32')
embeddings_index_rusvec = {}
for o in open('../input/wiki.ru.vec', encoding="utf8"):
    try:
        w,v = get_coefs(*o.strip().split(' '))
        if len(v) == 300:
            embeddings_index_rusvec[w] = v
    except:
        print(o)

all_embs = np.stack(embeddings_index_rusvec.values())
emb_mean,emb_std = all_embs.mean(), all_embs.std()

print('create embedding matrix rusvec')
word_index = tok_raw.word_index
nb_words = len(word_index) + 1
embedding_matrix_rusvec = np.random.normal(emb_mean, emb_std, (nb_words, embed_size))
for word, i in word_index.items():
    embedding_vector = embeddings_index_rusvec.get(word)
    try:
        if embedding_vector is not None: embedding_matrix_rusvec[i] = embedding_vector
        else: oov.append(word)
    except:
        print(word,i)
        pass

del embeddings_index_rusvec, all_embs, emb_std, emb_mean
gc.collect()

print("   Transforming text to seq...")
for c in ['context_0','context_1','context_2','reply', 'context']:
    train[c] = tok_raw.texts_to_sequences(train[c])
    test[c] = tok_raw.texts_to_sequences(test[c])

train['rep_len'] = train.reply.apply(len)
ss1 = MinMaxScaler()
train['rep_len'] = ss1.fit_transform(train.rep_len.values.reshape(-1, 1))
ss2 = MinMaxScaler()
train['repl_full_len'] = ss2.fit_transform(train.repl_full_len.values.reshape(-1, 1))
test['rep_len'] = test.reply.apply(len)
test['rep_len'] = ss1.transform(test.rep_len.values.reshape(-1, 1))
test['repl_full_len'] = ss2.transform(test.repl_full_len.values.reshape(-1, 1))

train['rel_len'] = train.reply.astype('str').str.count('\S+') / (1 + (train.context_2.astype('str') + ' ' + train.context_1.astype('str') + ' ' + train.context_0.astype('str')).str.count('\S+'))
train['rel_len0'] = train.reply.astype('str').str.count('\S+') / (1 + (train.context_0.astype('str')).str.count('\S+'))
ss3 = MinMaxScaler()
ss4 = MinMaxScaler()
train['rel_len'] = ss3.fit_transform(train.rel_len.values.reshape(-1, 1))
train['rel_len0'] = ss4.fit_transform(train.rel_len0.values.reshape(-1, 1))
test['rel_len'] = test.reply.astype('str').str.count('\S+') / (1 + (test.context_2.astype('str') + ' ' + test.context_1.astype('str') + ' ' + test.context_0.astype('str')).str.count('\S+'))
test['rel_len0'] = test.reply.astype('str').str.count('\S+') / (1 + (test.context_0.astype('str')).str.count('\S+'))
test['rel_len'] = ss3.transform(test.rel_len.values.reshape(-1, 1))
test['rel_len0'] = ss4.transform(test.rel_len0.values.reshape(-1, 1))
print(train.head(n=3))
print(test.head(n=3))

qtrain = pd.read_csv('../input/train_bpe.csv')
qtrain.columns = ['context_id','reply_id','context_bpe','reply_bpe']
train = train.merge(qtrain, on=['context_id','reply_id'], how='left')
qtest = pd.read_csv('../input/final_bpe.csv')
qtest.columns = ['context_id','reply_id','context_bpe','reply_bpe']
test = test.merge(qtest, on=['context_id','reply_id'], how='left')

def parse_int_list(s):
    s = s.strip()[1:-1].split(',')
    return [int(q) for q in s]

for c in ['reply_bpe','context_bpe']:
    train[c] = train[c].map(parse_int_list)
    test[c] = test[c].map(parse_int_list)

import numpy as np
embedding_matrix2 = np.load('../input/matrix.npy')
num_words2 = embedding_matrix2.shape[0]
print('num words 2:', num_words2)    

from keras.preprocessing.sequence import pad_sequences
def get_keras_data(dataset):
    X = {
        'context_0': pad_sequences(dataset.context_0, maxlen=30),
        'context_1': pad_sequences(dataset.context_1, maxlen=30),
        'context_2': pad_sequences(dataset.context_2, maxlen=30),
        'context': pad_sequences(dataset.context, maxlen=50),
        'context_bpe': pad_sequences(dataset.context_bpe, maxlen=40),
        'reply_bpe': pad_sequences(dataset.reply_bpe, maxlen=30),
        'reply': pad_sequences(dataset.reply, maxlen=30),
        'num_vars': np.array(dataset[["rep_len",'repl_full_len'
                                      # ,'cl_good_ftrvbpe','cl_bad_ftrvbpe','cl_neutral_ftrvbpe',
                                      #'rel_len0','rel_len','cnt_stopwords','rel_stops','rep_con_share_0','rep_con_share',
                                      #'dist_cos0','dist_jac0','dist_jaro','dist_jac','dist_cos'
                                      ]])
    }
    return X

from keras.layers import Input, Dropout, Dense, BatchNormalization,SpatialDropout1D, \
    Activation, concatenate, GRU, Embedding, Flatten, CuDNNGRU,Bidirectional,GlobalAveragePooling1D,GlobalMaxPooling1D,multiply,Conv1D
from keras.models import Model
from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping#, TensorBoard
from keras import backend as K
from keras import optimizers
from keras import initializers

dr = 0.25
def get_model():
    #params
    dr_r = dr
    
    #Inputs
    context0 = Input(shape=[x_train["context_0"].shape[1]], name="context_0")
    context1 = Input(shape=[x_train["context_1"].shape[1]], name="context_1")
    context2 = Input(shape=[x_train["context_2"].shape[1]], name="context_2")
    context_rusvec = Input(shape=[x_train["context"].shape[1]], name="context")
    context_bpe = Input(shape=[x_train["context_bpe"].shape[1]], name="context_bpe")
    reply_bpe = Input(shape=[x_train["reply_bpe"].shape[1]], name="reply_bpe")
    reply = Input(shape=[x_train["reply"].shape[1]], name="reply")
    num_vars = Input(shape=[x_train["num_vars"].shape[1]], name="num_vars")
    
    #Embeddings layers
    emb_context0 = SpatialDropout1D(0.1)(Embedding(nb_words, 300, weights=[embedding_matrix], trainable=False)(context0))
    emb_context1 = SpatialDropout1D(0.1)(Embedding(nb_words, 300, weights=[embedding_matrix], trainable=False)(context1))
    emb_context2 = SpatialDropout1D(0.1)(Embedding(nb_words, 300, weights=[embedding_matrix], trainable=False)(context2))
    emb_reply = SpatialDropout1D(0.1)(Embedding(nb_words, 300, weights=[embedding_matrix], trainable=False)(reply))

    emb_context_rusvec = SpatialDropout1D(0.1)(Embedding(nb_words, 300, weights=[embedding_matrix_rusvec], trainable=False)(context_rusvec))
    emb_reply_rusvec = SpatialDropout1D(0.1)(Embedding(nb_words, 300, weights=[embedding_matrix_rusvec], trainable=False)(reply))

    emb_context_bpe = SpatialDropout1D(0.1)(Embedding(num_words2, 50, weights=[embedding_matrix2], trainable=False)(context_bpe))
    emb_reply_bpe = SpatialDropout1D(0.1)(Embedding(num_words2, 50, weights=[embedding_matrix2], trainable=False)(reply_bpe))
    
    
    rnn_layer1 = Bidirectional(CuDNNGRU(32, return_sequences=True)) (emb_context0) #Bidirectional()
    rnn_layer2 = Bidirectional(CuDNNGRU(32, return_sequences=True)) (emb_context1) #Bidirectional()
    rnn_layer3 = Bidirectional(CuDNNGRU(32, return_sequences=True)) (emb_context2) #Bidirectional()
    rnn_layer4 = Bidirectional(CuDNNGRU(32, return_sequences=True))  (emb_reply)
    rnn_layer5 = Bidirectional(CuDNNGRU(32, return_sequences=True))  (emb_context_bpe)
    rnn_layer6 = Bidirectional(CuDNNGRU(32, return_sequences=True))  (emb_reply_bpe)

    rnn_layer7 = Bidirectional(CuDNNGRU(32, return_sequences=True))  (emb_context_rusvec)
    rnn_layer8 = Bidirectional(CuDNNGRU(32, return_sequences=True))  (emb_reply_rusvec)
    
    #main layer
    main_l1 = multiply([
        GlobalMaxPooling1D()(rnn_layer1),
        GlobalMaxPooling1D()(rnn_layer4)
    ])
    main_l2 = multiply([
        GlobalMaxPooling1D()(rnn_layer2),
        GlobalMaxPooling1D()(rnn_layer4)
    ])
    main_l3 = multiply([
        GlobalMaxPooling1D()(rnn_layer3),
        GlobalMaxPooling1D()(rnn_layer4)
    ])
    main_l4 = multiply([
        GlobalMaxPooling1D()(rnn_layer5),
        GlobalMaxPooling1D()(rnn_layer6)
    ])
    main_l5 = multiply([
        GlobalMaxPooling1D()(rnn_layer7),
        GlobalMaxPooling1D()(rnn_layer8)
    ])
    main_l = concatenate([main_l1,main_l2,main_l3,main_l4,main_l5,num_vars]) #
    main_l = Dropout(0.4)(main_l)
    
    #output
    output = Dense(1,activation="linear") (main_l)
    
    #model
    model = Model([context0,context1,context2,context_rusvec,context_bpe,reply_bpe,reply,num_vars], output) #
    optimizer = optimizers.RMSprop()
    #optimizer = optimizers.Adam(lr=0.0008)
    model.compile(loss="mse", 
                  optimizer=optimizer)
    return model

import math
def dpcg(df,col='yhat_tmp'):
    scores = []
    df2 = df.copy()
    df2['reply_id'] = df2.reply_id.astype(np.int32)
    df2['ylabel'] = df2.label.replace({'good':2,'neutral':1,'bad':0}).values.tolist()
    for g,r in df2.groupby('context_id'):
        r = r.sort_values('reply_id')
        lbls = r.ylabel.values.tolist()
        r = r.sort_values(col, ascending=False)
        dp = r.reply_id.values.tolist()
        i = 0
        score = 0
        for i in range(len(lbls)):
            score += lbls[dp[i]] / math.log2(2+i)
        iscore = 0
        lbls.sort()
        lbls.reverse()
        for i in range(len(lbls)):
            iscore += lbls[i] / math.log2(2+i)
        if iscore > 0:
            scores.append(score/iscore)
    return np.mean(scores)    

def _train_model(model, batch_size, train_x, train_y, val_x, val_y, df_val):
    best_loss = -1
    best_weights = None
    best_epoch = 0

    current_epoch = 0

    while True:
        #K.set_value(model.optimizer.lr, 0.001 * (0.9**current_epoch)) 
        model.fit(train_x, train_y, batch_size=batch_size, epochs=1)
        y_pred = model.predict(val_x, batch_size=batch_size)
        df_val.loc[:,'yhat_tmp'] = y_pred

        total_loss = dpcg(df_val)

        print("Epoch {0} loss {1} best_loss {2}".format(current_epoch, total_loss, best_loss))

        if (np.isnan(total_loss)):
            break

        current_epoch += 1
        if total_loss > best_loss or best_loss == -1:
            best_loss = total_loss
            best_weights = model.get_weights()
            best_epoch = current_epoch
        else:
            if current_epoch - best_epoch == 3:
                break

    model.set_weights(best_weights)
    return model    

import random
uids = train.context_id.unique().tolist()
#random.shuffle(uids)
BATCH_SIZE = 1024

for c in ['yhat','yhat_tmp']:
    train[c] = 0
    test[c] = 0
x_test = get_keras_data(test)

NUM_FOLDS = 10
NUM_K = 5

lrs = ['yhat_'+str(i) for i in range(50)]
lrs_train = ['yhat_'+str(i) for i in range(5)]

def blend_predicts_ranks(df, cols=lrs_train, res_col='yhat'):
    df2 = df.copy()
    df2['reply_id'] = df2.reply_id.astype(np.int32)
    df2['ylabel'] = df2.label.replace({'good':2,'neutral':1,'bad':0}).values.tolist()
    scores = []
    for g,r in df2.groupby('context_id'):
        r = r.sort_values('reply_id')
        lbls = r.ylabel.values.tolist()
        r['lgb_rank'] = 0
        for f in cols:
            r['lgb_rank'] += r[f].rank() / len(lrs)
        r = r.sort_values('lgb_rank', ascending=False)
        dp = r.reply_id.values.tolist()
        i = 0
        score = 0
        for i in range(len(lbls)):
            score += lbls[dp[i]] / math.log2(2+i)
        iscore = 0
        lbls.sort()
        lbls.reverse()
        for i in range(len(lbls)):
            iscore += lbls[i] / math.log2(2+i)
        if iscore > 0:
            scores.append(score/iscore)
    return np.mean(scores)

from sklearn.model_selection import KFold
test_pred = 0
ifold = 0
kf = KFold(n_splits=NUM_FOLDS, shuffle=True, random_state=239)
for train_index,test_index in kf.split(uids):
    val_ids = [uids[q] for q in test_index]
    val_df = train.loc[train.context_id.isin(val_ids),:]
    y_train,y_valid = train.loc[~train.context_id.isin(val_ids),'y'].values,train.loc[train.context_id.isin(val_ids),'y'].values
    x_train,x_valid = get_keras_data(train.loc[~train.context_id.isin(val_ids),:]),get_keras_data(val_df)
    for k in range(NUM_K):
	    K.clear_session()
	    model = get_model()
	    model = _train_model(model, BATCH_SIZE, x_train, y_train, x_valid,y_valid, val_df[['context_id','reply_id','label']])
	    train.loc[train.context_id.isin(val_ids),'yhat_'+str(k)] = model.predict(get_keras_data(val_df), batch_size=BATCH_SIZE).ravel()
	    p = model.predict(x_test, batch_size=BATCH_SIZE)
	    test['yhat_'+str(test_pred)] = p.ravel()
	    test_pred = test_pred + 1
    print(ifold, blend_predicts_ranks(train.loc[train.context_id.isin(val_ids),:]))
    ifold = ifold + 1

lrs = ['yhat_'+str(i) for i in range(50)]
lrs_train = ['yhat_'+str(i) for i in range(5)]
def blend_predicts(df, cols, res_col='yhat'):
    d = []
    df2 = df.copy()
    df2['reply_id'] = df2.reply_id.astype(np.int32)
    scores = []
    for g,r in df2.groupby('context_id'):
        r = r.sort_values('reply_id')
        r[res_col] = 0
        for f in cols:
            r[res_col] += r[f].rank() / len(cols)
        for qr in range(len(r[res_col].values.tolist())):
            d.append({'reply_id':qr,'context_id':g,res_col:r[res_col].values.tolist()[qr]})    
    return pd.DataFrame(d)

qqq = blend_predicts(train,lrs_train)
if 'yhat' in train.columns:
	train = train.drop(['yhat'],axis=1)
train = train.merge(qqq,on=['context_id','reply_id'],how='left')
print(dpcg(train,'yhat'))
qqq = blend_predicts(test,lrs)
if 'yhat' in test.columns:
	test = test.drop(['yhat'],axis=1)
test = test.merge(qqq,on=['context_id','reply_id'],how='left')

qq = train[['context_id','reply_id','yhat']].copy()
qq.columns = ['context_id','reply_id','regr_3ft_rv_bpe']
qq.to_csv('train_regr3ft_rv_bpe.csv',index=False)

qq = test[['context_id','reply_id','yhat']].copy()
qq.columns = ['context_id','reply_id','regr_3ft_rv_bpe']
qq.to_csv('test_regr3ft_rv_bpe.csv',index=False)        

cids = []
rids = []
for g,r in test.groupby('context_id'):
    rr = r.sort_values('yhat',ascending=False).reply_id.values.tolist()
    for q in rr:
        cids.append(g)
        rids.append(q)
qqq = pd.DataFrame(columns=['context_id','reply_id'])
qqq['context_id'] = cids
qqq['reply_id'] = rids
qqq.to_csv('regression_3ft_rv_bpe.csv', sep='\t',header=False,index=False)